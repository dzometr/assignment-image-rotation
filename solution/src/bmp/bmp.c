#include <stdio.h>

#include "image/image.h"
#include "bmp/bmp.h"
#include "rotate/rotate.h"

#define BMP_HEADER_TYPE 19778
#define BMP_HEADER_PLANES 1
#define BMP_HEADER_INFO_SIZE 40

struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
} __attribute__((packed));

static enum read_status read_header(FILE* in, struct bmp_header* header) {
    if (fseek(in, 0, SEEK_END) != 0) {
        return READ_STREAM_TROUBLE;
    }
    size_t size = ftell(in);
    if (size < sizeof(struct bmp_header)) 
        return READ_INVALID_HEADER;
    rewind(in);
    if (fread(header, sizeof(struct bmp_header), 1, in) != 1) {
        return READ_STREAM_TROUBLE;
    }
    return READ_OK;
}

static enum read_status read_pixels(struct image* img, FILE* in, uint8_t padding) {
    struct image fully_readed_image = image_create(img->width, img->height);
    struct pixel *pixels = fully_readed_image.pixels;
    for (size_t i = 0; i < img->height; i++) {
        if (fread(pixels + i * img->width, sizeof(struct pixel), img->width, in) != img->width) {
            return READ_STREAM_TROUBLE;
        }
        if (fseek(in, padding, SEEK_CUR) != 0) {
            return READ_STREAM_TROUBLE;
        }
    }
    img->pixels = pixels;
    return READ_OK;
}


static inline uint8_t get_padding(uint32_t width) { return width % 4; }


enum read_status from_bmp(FILE* in, struct image* img) {
    struct bmp_header header = {0};
    if (read_header(in, &header) != READ_OK) 
        return READ_INVALID_SIGNATURE;
    if (header.bfType != BMP_HEADER_TYPE ||
        header.biPlanes != BMP_HEADER_PLANES||
        header.biSize != BMP_HEADER_INFO_SIZE
    ) return READ_INVALID_HEADER;
    img->width = header.biWidth;
    img->height = header.biHeight;
    return read_pixels(img, in, get_padding(header.biWidth));
}

static struct bmp_header create_header(const struct image img) {
    const uint32_t img_size = (sizeof(struct pixel) * img.width + get_padding(img.width)) * img.height;
    struct bmp_header header = {
            .bfType = BMP_HEADER_TYPE,
            .bfileSize = sizeof(struct bmp_header) + img_size,
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = BMP_HEADER_INFO_SIZE,
            .biWidth = img.width,
            .biHeight = img.height,
            .biPlanes = BMP_HEADER_PLANES,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = img_size,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0,
    };
    return header;
}

enum write_status write_image(struct image const* img, FILE* out, const uint8_t padding) {
    const uint64_t zero = 0;
    uint64_t count = 0;
    for (size_t i = 0; i < img->height; ++i) {
        size_t const write_result = fwrite(img->pixels + i * img->width, sizeof(struct pixel), img->width, out);
        if (write_result != img->width) {
            return WRITE_ERROR;
        }
        count += (uint64_t) write_result;
        size_t const write_result_zero = fwrite(&zero, 1, padding, out);
        if (write_result_zero != padding) {
            return WRITE_ERROR;
        }
    }
    if (count != img->height * img->width) 
        return WRITE_ERROR;
    return WRITE_OK;
}

enum write_status to_bmp(FILE* out, struct image const* img) {
    struct bmp_header header = create_header(*img);
    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) 
        return WRITE_ERROR;
    const uint8_t padding = get_padding(header.biWidth);
    return write_image(img, out, padding);
}

