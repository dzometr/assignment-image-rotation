#include "bmp/bmp.h"
#include "image/image.h"
#include "rotate/rotate.h"

#include <stdio.h>

int main(int args, char** argv) {
    if (args != 3) {
        printf("%s", "Not enough arguments");
        return 77;
    }
    FILE* in = fopen(argv[1], "rb");
    FILE* out = fopen(argv[2], "wb");
    if (!in){
        printf("%s", "Unable to open the input file");
        return 66;
    }
    if (!out){
        printf("%s", "Unable to open the output file");
        return 55;
    }
    struct image image = {0, 0, NULL};
    if (from_bmp(in, &image) != READ_OK) {
        image_destroy(&image);
        printf("%s", "Unable to read the input file");
        if (fclose(in)) {
            printf("%s", "Unable to close input file");
            return 33;
        }
        return 44;
    }
    if (fclose(in)) {
        image_destroy(&image);
        printf("%s", "Unable to close input file");
        if (fclose(out)) {
            printf("%s", "Unable to close output file");
            return 11;
        }
        return 33;
    }
    struct image rotated_image = rotate(&image);
    image_destroy(&image);
    if (to_bmp(out, &rotated_image) != WRITE_OK) {
        image_destroy(&rotated_image);
        printf("%s", "Unable to write in the output file");
        if (fclose(out)) {
            printf("%s", "Unable to close output file");
            return 11;
        }
        return 22;
    }
    if (fclose(out)) {
        image_destroy(&rotated_image);
        printf("%s", "Unable to close output file");
        return 11;
    }
    image_destroy(&rotated_image);
    return 0;
}




