#include "rotate/rotate.h"

struct image rotate(struct image const* source_image) {
    struct image result_image = image_create(source_image->height, source_image->width);
    for (size_t i = 0; i < source_image->width; ++i) {
        for (size_t j = 0; j < source_image->height; ++j) {  
            result_image.pixels[image_get_pixel_index_by_coordinates(i, j, result_image.width)] = (source_image->pixels[image_get_pixel_index_by_coordinates(result_image.width-j-1, i, source_image->width)]);
        }
    }
    return result_image;
}

