#include "image/image.h"

void image_destroy(const struct image *img) {
    free(img->pixels);
}

struct image image_create(size_t width, size_t height) {
    struct image result_image = {0, 0, NULL};
    result_image.width = width;
    result_image.height = height;
    result_image.pixels = malloc(width * height * sizeof(struct pixel));
    return result_image;
}

size_t image_get_pixel_index_by_coordinates(size_t i, size_t j, size_t width){
    return i * width + j;
}

