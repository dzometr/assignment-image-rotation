#pragma once

#include "image/image.h"

#include  <stdint.h>

enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_HEADER,
    READ_STREAM_TROUBLE,
    /* коды других ошибок  */
};

enum read_status from_bmp(FILE* in, struct image* img);

/*  serializer   */
enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR
    /* коды других ошибок  */
};

enum write_status to_bmp(FILE* out, struct image const* img);

enum compression_types {
    BI_RGB = 0,
    BI_RLE8 = 1,
    BI_RLE4 = 2,
    BI_BITFIELDS = 3,
    BI_JPEG = 4,
    BI_PNG = 5,
    BI_A
};

