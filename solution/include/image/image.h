#pragma once

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

struct image {
    uint64_t width, height;
    struct pixel* pixels;
};

struct pixel {
    uint8_t b, g, r;
}__attribute__((packed));

void image_destroy(const struct image *img);
struct image image_create(size_t width, size_t height);
size_t image_get_pixel_index_by_coordinates(size_t i, size_t j, size_t width);

